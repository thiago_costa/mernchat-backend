import express, { Request, Response } from 'express';
import dotenv from 'dotenv';
import authRoutes from './routes/auth.routes';
import messageRoutes from './routes/message.routes';
import userRoutes from './routes/user.routes';

import connectToMongoDB from './db/connector';
dotenv.config();

const app = express();
app.use(express.json());

const port = process.env.PORT || 5000;

app.get('/check', (req: Request, res: Response) => {
  res.status(200).json({ success: true, Date });
});

app.use('/api/auth', authRoutes);
app.use('/api/message', messageRoutes);
app.use('/api/user', userRoutes);

app.listen(port, () => {
  // connectToMongoDB();
  console.log('Server running');
});
