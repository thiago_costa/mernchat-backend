import mongoose from 'mongoose';

const connectToMongoDB = async () => {
  try {
    const uri = process.env.MONGO_DB_URI!;
    await mongoose.connect(uri);
    console.log('Database connecion stablished');
  } catch (error) {
    console.log(error);
    console.log('Error on coonnect to database' + error);
  }
};

export default connectToMongoDB;
