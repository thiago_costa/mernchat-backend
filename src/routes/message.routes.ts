import express from 'express';
import { getMessage, sendMessage } from '../controllers/message.controller';
import protectRoute from '../middleware/protectRoute';

const router = express.Router();

router.post('/:id', protectRoute, getMessage);
router.post('/send/:id', protectRoute, sendMessage);

export default router;
