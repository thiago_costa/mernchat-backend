import express from 'express';
import protectRoute from '../middleware/protectRoute';
import { getChatUsers } from '../controllers/user.controller';

const router = express.Router();

router.get('/', protectRoute, getChatUsers);

export default router;
