import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import User from '../models/user.model';
import generateTokenAndSetCookie from '../utils/generateToken';

export const signup = async (req: Request, res: Response) => {
  try {
    const { username, email, password, confirmPassword } = req.body;

    if (password != confirmPassword) {
      return res.status(400).json({ error: "Passwords don't match" });
    }

    const user = await User.findOne({ email });

    if (user) {
      return res.status(400).json({ error: 'Email already in use' });
    }

    const profilePic = `https://avatar.iran.liara.run/username?username=${username}`;

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const newUser = new User({
      username,
      email,
      password,
      profilePic: profilePic,
    });

    if (newUser) {
      generateTokenAndSetCookie(newUser._id.toString(), res);
      await newUser.save();

      res.status(201).json({
        id: newUser._id,
        username: newUser.username,
        profilePic: newUser.profilePic,
      });
    } else {
      res.status(500).json({
        error: 'Invalid user data',
      });
    }
  } catch (err) {
    res.status(500).json({
      error: 'Error on crete user',
    });
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    const isPasswordCorrect = await bcrypt.compare(
      password,
      user?.password || '',
    );

    if (!user || !isPasswordCorrect) {
      return res.status(400).json({ error: 'Invalid username or passowrd' });
    }

    generateTokenAndSetCookie(user._id.toString(), res);

    res.status(201).json({
      id: user._id,
      email: user.email,
      username: user.username,
      profilePic: user.profilePic,
    });
  } catch (err) {
    res.status(500).json({
      error: 'Error on login',
    });
  }
};

export const logout = (req: Request, res: Response) => {
  try {
    res.cookie('jwt', '', { maxAge: 0 });
    res.status(200).json({ message: 'Logged out successfully' });
  } catch (error) {
    console.log('Error on logou', error);

    res.status(500).json({ error: 'Internal server error ' });
  }
};
