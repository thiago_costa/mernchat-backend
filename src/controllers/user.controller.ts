import express, { Request, Response } from 'express';
import User from '../models/user.model';

export const getChatUsers = async (req: Request, res: Response) => {
  try {
    const loggedUserId = req.user.id;

    const allUsers = await User.find({ _id: { $ne: loggedUserId } }).select(
      '-password',
    );

    res.status(200).json(allUsers);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};
