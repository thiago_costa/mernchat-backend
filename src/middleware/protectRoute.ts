import { Request, Response, NextFunction } from 'express';
import jwt, { JwtPayload } from 'jsonwebtoken';
import User from '../models/user.model';

const protectRoute = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const token = req.cookies.jwt;
    if (!token) {
      return res
        .status(401)
        .json({ error: 'Unauthorized - No token provided ' });
    }

    const decoded = jwt.verify(token, process.env.JWT_SECRET!) as JwtPayload;

    if (!decoded) {
      return res.status(401).json({ error: 'Unauthorized - Invalid token ' });
    }

    const user = await User.findById(decoded.userId).select('-password');

    req.user = user;

    next();
  } catch (error) {
    console.log('Error in protected routes middleware', error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

export default protectRoute;
